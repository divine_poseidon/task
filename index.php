﻿<?php
session_start();
require_once 'PHP/functions.php';

   if(!isset($_SESSION['login']) && !isset($_SESSION['token']) && !isset($_SESSION['user_type']))
   {
       header("Location: PHP/login.php");
   }


   ?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>M0XN0</title>
    <script src="src/jquery/jquery.js"></script>
    <link rel="stylesheet" href="src/semantic/semantic.min.css">
    <script src="src/semantic/semantic.min.js"></script>
    <link rel="stylesheet" href="src/css/icon.min.css">
    <link rel="stylesheet" href="src/css/style.css">

    <script src="src/js/main.js"></script>

</head>
<body>
<a href="PHP/admin.php" class="admin_page">ADMIN</a>
</body>
</html>
