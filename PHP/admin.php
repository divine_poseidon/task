<?php
require_once 'functions.php';
session_start();


if(!isset($_SESSION['login']) && !isset($_SESSION['token']) && !isset($_SESSION['user_type']))
{
    header("Location: login.php");

}
else
{
    verify_admin();
}

?>

<h1>ADMIN PAGE!</h1>


<form type="GET" action="admin.php">
    <span>Search</span>
    <input type="text" name="search">
    <input type="submit" name="submit">
</form>

<?php

    if(isset($_GET['search']))
    {
        $response = search_movies($_GET['search']);

        foreach ($response as $key => $value)
        {
            echo $response[$key]->title_en ."<br>";
            echo "<iframe src=".$response[$key]->iframe_url." width=\"610\" height=\"370\" frameborder=\"0\" allowfullscreen></iframe><br>";
            echo $response[$key]->material_data->description;
            if($key == 3)
            {
                return 0;
            }
        }

    }
?>