<?php

require_once 'functions.php';

$response = [];

if(!empty($_POST))
{
    if(isset($_POST['login']) && isset($_POST['password']))
    {
        $login = $_POST['login'];
        $password = $_POST['password'];

        $login = trim($login);
        $password = trim($password);

        $uniqueness_check_result = check_uniquness($login);

        if($uniqueness_check_result === false)
        {
            register_user($login, $password);
            $response['success'] = true;
        }
        else
        {
            $response['success'] = false;
            $response['login'] = 'there is another user with this login';
        }

        echo json_encode($response);

    }
}
