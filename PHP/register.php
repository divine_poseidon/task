<?php
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <script src="../src/jquery/jquery.js"></script>
    <link rel="stylesheet" href="../src/semantic/semantic.min.css">
    <script src="../src/semantic/semantic.min.js"></script>
    <link rel="stylesheet" href="../src/css/icon.min.css">
    <link rel="stylesheet" href="../src/css/style.css">

    <script src="../src/js/main.js"></script>

</head>
<body>

    <div class="register_form">
        <input type="text" id="register_login">
        <input type="password" id="register_password">
        <input type="submit" id="register_submit">
    </div>


</body>
</html>