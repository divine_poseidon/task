<?php

function hash_password($password)
{
    $result = password_hash($password, PASSWORD_DEFAULT);

    return $result;
}

function database_connect()
{
    $servername = "localhost";
    $username = "root";
    $password = "";

        $conn = new PDO("mysql:host=$servername;dbname=task", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
}

function check_uniquness($login)
{
    $pdo = database_connect();

    $query = $pdo->prepare('SELECT COUNT(*) FROM users WHERE login = :login');

    $query->bindParam(':login', $login, PDO::PARAM_STR);

    $query->execute();

    if ($query->fetchColumn() > 0)
    {

        $check_result = (bool) true;

        return $check_result;
    }
    else
    {
        $check_result = (bool) false;

        return $check_result;
    }
}

function register_user($login, $password)
{
    $pdo = database_connect();
    $hashed_password = hash_password($password);
    $token = generate_random_token();

    $query = $pdo->prepare('INSERT INTO users(login, password, token) VALUES(:login, :password, :token)');

    $query->bindParam(':login', $login, PDO::PARAM_STR);
    $query->bindParam(':password', $hashed_password, PDO::PARAM_STR);
    $query->bindParam(':token', $token, PDO::PARAM_STR);

    $query->execute();

}

function login_user($login)
{
    session_start();

    $token = get_user_token($login);
    $user_type = check_if_admin($login);

    if($user_type === 0)
    {
        $_SESSION['login'] = $login;
        $_SESSION['token'] = $token;
        $_SESSION['user_type'] = 'user';
    }
    else
    {
        $_SESSION['login'] = $login;
        $_SESSION['token'] = $token;
        $_SESSION['user_type'] = 'admin';

    }


}

function search_movies($title)
{
    $key = '88fe62b2e80a57e376b843c208fbccb1';

    $page = 1;
    if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, "http://moonwalk.cc/api/videos.json?title=$title&api_token=$key");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $out = curl_exec($curl);
        curl_close($curl);
    }

    $out = json_decode($out);

    return $out;


}

function generate_random_token()
{
    $token = uniqid(md5(random_bytes(20)));

    return $token;

}

function get_user_token($login)
{
    $pdo = database_connect();

    $query = $pdo->prepare('SELECT token FROM users WHERE login = :login');

    $query->bindParam(':login', $login, PDO::PARAM_STR);

    $query->execute();

    $result = $query->fetch();

    $user_token = $result['token'];

    return $user_token;
}

function check_if_admin($login)
{
    $pdo = database_connect();

    $query = $pdo->prepare('SELECT is_admin FROM users WHERE login = :login');

    $query->bindParam(':login', $login, PDO::PARAM_STR);

    $query->execute();

    $result = $query->fetch();

    $user_status = $result['is_admin'];

    return $user_status;
}

function verify_admin()
{
    $login = $_SESSION['login'];
    $token = $_SESSION['token'];
    $user_type = $_SESSION['user_type'];

    if($user_type != 'admin')
    {
        $table_token = get_user_token($login);

        if($table_token != $token)
        {
            header("Location: ../index.php");
        }
    }

}

