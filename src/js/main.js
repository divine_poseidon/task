$(document)
    .ready(function () {

        $("#login_submit").on("click", function()
        {
            //BLOCK "SUBMIT" BUTTON
            $("#login_submit").prop("disabled", true);

            // AJAX QUERY
            $.ajax
            ({
                url: "login_process.php",
                method: 'post',
                cache: false,
                data: {
                    login: $("#login_login").val(),
                    password: $("#login_password").val()
                }
            }).done(function(data)
            {
                $("#login_submit").prop("disabled", false);

                let response = JSON.parse(data);

                if(response === 'succeed')
                {
                    alert('SUCCEED');
                    window.location.reload();
                }

            });
        });

        $("#register_submit").on("click", function()
        {
            //BLOCK "SUBMIT" BUTTON
            $("#register_submit").prop("disabled", true);

            // AJAX QUERY
            $.ajax
            ({
                url: "register_process.php",
                method: 'post',
                cache: false,
                data: {
                    login: $("#register_login").val(),
                    password: $("#register_password").val()
                }
            }).done(function(data)
            {
                $("#register_submit").prop("disabled", false);

                let response = JSON.parse(data);

                if(response === 'succeed')
                {
                    window.location.reload();
                }

            });
        });
});
